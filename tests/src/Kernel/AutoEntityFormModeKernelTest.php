<?php

namespace Drupal\Tests\auto_entity_form_mode\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests that for mode displays are automatically picked up.
 *
 * @group auto_entity_form_mode
 */
class AutoEntityFormModeKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'auto_entity_form_mode',
    'auto_entity_form_mode_test',
    'node',
    'system',
    'taxonomy',
    'text',
    'user',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('user');
    $this->installConfig(['auto_entity_form_mode_test']);
    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->entityTypeManager->clearCachedDefinitions();
  }

  /**
   * Tests that for mode displays are automatically picked up.
   *
   * @dataProvider providerAutoEntityFormMode
   */
  public function testAutoEntityFormMode(string $entity_type_id, string $entity_form_display_id): void {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $this->assertTrue($entity_type->hasHandlerClass('form', $entity_form_display_id));
  }

  /**
   * Data provider for testAutoEntityFormMode().
   *
   * @see testAutoEntityFormMode()
   */
  public function providerAutoEntityFormMode(): array {
    return [
      [
        'node',
        'my_custom_node_form_display',
      ],
      [
        'taxonomy_term',
        'my_custom_taxonomy_term_form_display',
      ],
      [
        'user',
        'my_custom_user_form_display',
      ],
    ];
  }

}
